import com.facebook.presto.sql.tree.QualifiedName;

import java.util.Comparator;

/**
 * Comparator for QualifiedNames.
 */
public class QualifiedNameComparator implements Comparator<QualifiedName> {

    /**
     * This method compares two QualifiedNames (required for a ListMultimap) by comparing their prefixes and suffixes
     * based on lexicographical order (case-insensitive) of their String representations.
     * @param o1 First QualifiedName
     * @param o2 Second QualifiedName
     * @return Comparison value
     */
    @Override
    public int compare(QualifiedName o1, QualifiedName o2) {
        return o1.toString().compareToIgnoreCase(o2.toString());
    }
}
