import com.facebook.presto.sql.QueryUtil;
import com.facebook.presto.sql.parser.ParsingOptions;
import com.facebook.presto.sql.parser.SqlParser;
import com.facebook.presto.sql.tree.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QueryAnalysisUtils {


    private ArrayList<Expression> additionalExpressions;

// ############################## Constructors #############################################

    public QueryAnalysisUtils() {
        additionalExpressions = new ArrayList<>();
    }


// ################################# Utils #################################################

    /**
     * Returns SELECT clause as {@link Select} from QueryBody
     * @param queryBody Query
     * @return SELECT clause
     */
    public Select getSelect(QueryBody queryBody) {
        return ((QuerySpecification) queryBody).getSelect();
    }


    /**
     * Returns the FROM clause as {@link Relation} if it is present; null is returned otherwise
     * @param queryBody Query
     * @return FROM relation or null
     */
    public Relation getFrom(QueryBody queryBody) {
        Optional<Relation> optFrom = ((QuerySpecification) queryBody).getFrom();
        return optFrom.orElse(null);
    }


    /**
     * Returns the WHERE clause as {@link Expression} if it is present; null is returned otherwise
     * @param queryBody Query
     * @return WHERE expression or null
     */
    public Expression getWhere(QueryBody queryBody) {
        Optional<Expression> optWhere = ((QuerySpecification) queryBody).getWhere();
        return optWhere.orElse(null);
    }


    /**
     * Returns the JoinCriteria of a given Join if present; otherwise null is returned
     * @param join Join
     * @return JoinCriteria or null
     */
    public JoinCriteria getJoinCriteria(Join join) {
        Optional<JoinCriteria> optCriteria = join.getCriteria();
        return optCriteria.orElse(null);
    }


    /**
     * Parse the given SQL-Query with the {@link SqlParser} to a {@link QueryBody}.
     * @param sql SQL-Query as String
     * @return Parsed QueryBody
     */
    public QueryBody parseQueryBody(String sql) {
        SqlParser parser = new SqlParser();
        Query query = (Query) parser.createStatement(sql, new ParsingOptions());
        return query.getQueryBody();
    }


    /**
     * Builds a String representation to the given SELECT clause
     * @param select SELECT clause
     * @return String representation
     */
    public String selectToString(Select select) {
        if (select == null)
            throw new IllegalArgumentException("Select clause " + select + " must not be null!");

        StringBuilder sb = new StringBuilder("SELECT ");
        if (select.isDistinct())
            sb.append("DISTINCT ");

        List<SelectItem> items = select.getSelectItems();
        if (items.size() == 0) {
            throw new IllegalArgumentException("Select items list must not be empty!");
        }
        else if (items.size() == 1) {
            sb.append(items.get(0));
        } else {
            sb.append(items.get(0));
            for (int i = 1; i < items.size(); i++) {
                sb.append(", ").append(items.get(i));
            }
        }
        return sb.toString();
    }


    /**
     * Builds a String representation to the given FROM clause
     * @param from FROM clause
     * @return String representation
     */
    public String fromToString(Relation from) {
        return "FROM " + relationToString(from);
    }


    /**
     * Builds a String representation to the given Relation (either a Join or a Table, else rejected)
     * @param rel Relation (e.g. FROM clause)
     * @return String representation
     */
    public String relationToString(Relation rel) {
        if (rel == null)
            throw new IllegalArgumentException("Relation " + rel + " must not be null!");


        // Differentiate Join (multiple relations) and Table (single relation); reject other types
        StringBuilder sb = new StringBuilder();
        if (rel instanceof Join) {
            sb.append(nestedJoinsToString((Join) rel));
        } else if (rel instanceof Table) {
            Table table = (Table) rel;
            sb.append(table.getName().toString().toUpperCase());
        } else
            throw new IllegalArgumentException("Given relation " + rel + " is not of type Join or Table but of " +
                    "type " + rel.getClass().getName());

        return sb.toString();
    }


    /**
     * Builds a String representation to the given (possibly nested) Join
     * @param join Join
     * @return String representation
     */
    public String nestedJoinsToString(Join join) {
        if (join == null)
            throw new IllegalArgumentException("Join " + join + " must not be null!");

        StringBuilder sb = new StringBuilder();

        // Get left and right part of join and append relations' String representations
        Relation left = join.getLeft();
        Relation right = join.getRight();
        sb.append(relationToString(left)).append(", ").append(relationToString(right));

        // If JoinCriteria are present, add them to the list of additional expressions to print in WHERE clause later
        // or ignore them (if it is a natural join)
        JoinCriteria criteria = getJoinCriteria(join);
        if (criteria != null) {
            if (criteria instanceof JoinOn) {
                // get the expression of the JoinOn criteria & add it
                JoinOn joinOn = (JoinOn) criteria;
                this.additionalExpressions.add(joinOn.getExpression());
            } else if (criteria instanceof JoinUsing) {
                // create ComparisonExpressions from the column list [c1, c2, c3, ... ] -> c1 = c2, c2 = c3, c3 = ...
                // and add all of them to the additional expressions list
                JoinUsing joinUsing = (JoinUsing) criteria;
                List<Identifier> columns = joinUsing.getColumns();
                for (int i = 0; i < columns.size() - 1; i++) {
                    Identifier col = columns.get(i);
                    Identifier nextcol = columns.get(i+1);
                    ComparisonExpression e = new ComparisonExpression(ComparisonExpression.Operator.EQUAL, col, nextcol);
                    this.additionalExpressions.add(e);
                }
            } else if (criteria instanceof NaturalJoin) {
                //No-op, because natural join has no explicit join conditions.
            } else
                throw new IllegalArgumentException("JoinCriteria of the Join " + join + " are not of type JoinOn, " +
                        "JoinUsing or NaturalJoin but of type " + criteria.getClass());
        }

        return sb.toString();
    }


    /**
     * Builds a String representation to the given WHERE expression (plus possibly additional expressions from FROM,
     * because syntactical sugar, e.g. "A JOIN B ON x = y" is transformed to a WHERE expression)
     * @param where WHERE clause
     * @return String representation
     */
    public String whereToString(Expression where) {
        if (where == null)
            return "";      // WHERE may be null

        for (Expression e : additionalExpressions)
            where = QueryUtil.logicalAnd(where, e);
        return "WHERE " + where.toString();
    }


    /**
     * Builds a String representation to the given query in the simple format "SELECT ... FROM ... WHERE ..."
     * @param q Query
     * @return String representation of the query
     */
    public String queryToString(Query q) {
        StringBuilder sb = new StringBuilder();
        QueryBody queryBody = q.getQueryBody();

        sb.append(selectToString(getSelect(queryBody))).append(" ")
                .append(fromToString(getFrom(queryBody))).append(" ")
                .append(whereToString(getWhere(queryBody)));
        return sb.toString();
    }


    /**
     * Test unit
     * @param args Not used
     */
    public static void main(String[] args) {

        // Test: SQL string -> SqlParser -> Query object -> queryToString -> same SQL String
        QueryAnalysisUtils utils = new QueryAnalysisUtils();
        String sql = "SELECT * FROM TA JOIN TB ON TA.ID = TB.IDOFA JOIN TC ON TA.ID = TC.WHATEVER WHERE TA.AGE > 20";
        SqlParser parser = new SqlParser();
        Query query = (Query) parser.createStatement(sql, new ParsingOptions());
        System.out.println("Original SQL: " + sql);
        System.out.println("Parsed and retransformed SQL: " + utils.queryToString(query));

        // Build simple query and test again
        Select select = QueryUtil.selectList(new AllColumns());

        Table ta = new Table(QualifiedName.of("TA"));
        Table tb = new Table(QualifiedName.of("TB"));
        List<Identifier> columns = new ArrayList<>();
        columns.add(new Identifier("ID"));
        columns.add(new Identifier("IDOFA"));
        JoinCriteria criteria = new JoinUsing(columns);
        Relation from = new Join(Join.Type.INNER, ta, tb, Optional.of(criteria));

        Expression left = DereferenceExpression.from(QualifiedName.of("TA.AGE"));
        Expression right = new LongLiteral("20");
        ComparisonExpression where = new ComparisonExpression(ComparisonExpression.Operator.GREATER_THAN, left, right);

        query = QueryUtil.simpleQuery(select, from, where);
        System.out.println("Parsed and retransformed SQL: " + utils.queryToString(query));
    }


}
