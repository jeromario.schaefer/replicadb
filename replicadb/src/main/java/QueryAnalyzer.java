import com.facebook.presto.sql.QueryUtil;
import com.facebook.presto.sql.TreePrinter;
import com.facebook.presto.sql.parser.ParsingOptions;
import com.facebook.presto.sql.parser.SqlParser;
import com.facebook.presto.sql.tree.*;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.MultimapBuilder;
import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;
import org.jetbrains.annotations.NotNull;

import javax.swing.text.html.Option;
import java.sql.*;
import java.sql.Statement;
import java.util.*;
import java.util.List;

import static com.facebook.presto.sql.tree.ComparisonExpression.*;

/**
 * This class provides the analysis a query. It extends the class {@link DefaultExpressionTraversalVisitor} to traverse
 * the given SQL-Query with the provided method {@link QueryAnalyzer#analyzePrint(String)}. ...
 */
public class QueryAnalyzer extends DefaultExpressionTraversalVisitor<Void, Void> {

    /**
     * Stores comparison expressions
     */
    private ArrayList<ComparisonExpression> comparisons;

    /**
     * Stores the joins in the analyzed query found in WHERE clause
     */
    private ArrayList<Join> joins;

    /**
     * Stores possible fragment candidates to analyze if there are fragmentations that can match that selections
     */
    private ArrayList<ComparisonExpression> fragCandidates;

    /**
     * Saves the orginal SQL query that was the last input to the analysis method
     */
    private String inputSqlQuery;

    /**
     * Rewritten FROM clause for new query
     */
    private Relation rewrittenFrom;

    /**
     * List of all expressions that are used for the new WHERE clause
     */
    private ArrayList<Expression> rewrittenWhere;

    /**
     * Connection to DB
     */
    private Connection conn;


    private QueryAnalysisUtils utils;

//########################### Constructors ###################################

    /**
     * Init
     *
     * @param conn Connection to one of the databases (for metadata table access)
     */
    public QueryAnalyzer(Connection conn) {
        super();
        this.comparisons = new ArrayList<>();
        this.joins = new ArrayList<>();
        this.fragCandidates = new ArrayList<>();
        this.inputSqlQuery = null;
        this.rewrittenFrom = null;
        this.rewrittenWhere = null;
        this.utils = null;

        this.conn = conn;
    }


//########################### Overwritten Methods ###################################

    /**
     * When visiting a {@link ComparisonExpression}, the expression is stored into the HashMap for analysis
     *
     * @param node    ComparisonExpression that is visited
     * @param context Not needed
     * @return Nothing
     */
    @Override
    protected Void visitComparisonExpression(ComparisonExpression node, Void context) {

        // Store in hashmap to analyze after where condition traversal
        comparisons.add(node);

        return super.visitComparisonExpression(node, context);
    }

    @Override
    protected Void visitDereferenceExpression(DereferenceExpression node, Void context) {
        return super.visitDereferenceExpression(node, context);
    }

    @Override
    protected Void visitValues(Values node, Void context) {
        return super.visitValues(node, context);
    }

//########################### Getter & Setter ###################################

    public ArrayList<ComparisonExpression> getComparisons() {
        return comparisons;
    }

    public String getInputSqlQuery() {
        return inputSqlQuery;
    }

    public ArrayList<Join> getJoins() {
        return joins;
    }

    public ArrayList<ComparisonExpression> getFragCandidates() {
        return fragCandidates;
    }


//########################### Analysis Methods ###################################

    /**
     * Analyze a given SQL query and print the results
     *
     * @param sql SQL query
     * @return True, if the query could be analyzed; false otherwise
     */
    public boolean analyzePrint(String sql) {

        // update class variables
        this.comparisons.clear();
        this.fragCandidates.clear();
        this.joins.clear();
        this.inputSqlQuery = sql;
        this.utils = new QueryAnalysisUtils();


        // parsing the expression with presto-parser to obtain the query body
        QueryBody queryBody = this.utils.parseQueryBody(sql);

        // Analyze FROM expression (should be present, otherwise throw an exception)
        if (analyzeFromExpression(queryBody)) {
            System.out.println("FROM expression was successfully analyzed!");
        } else {
            // false means no join is present so only select from one table (no co-partitioning)
            System.out.println("FROM expression was be analyzed but no join was found!");
        }

        // Analyze WHERE expression (if present)
        ListMultimap<QualifiedName, Integer> frags = null;
        if (analyzeWhereExpression(queryBody)) {
            System.out.println("WHERE expression was successfully analyzed!");

            // process possible fragment candidates obtained from selection conditions from WHERE analysis
            if (!fragCandidates.isEmpty()) {
                System.out.println("Testing " + fragCandidates.size() + " fragment candidates ... ");       // DEBUG
                frags = testFragCandidates();
                System.out.println(frags.size() + " fragments found!");   // DEBUG
                if (frags.isEmpty()) {
                    // There are no fragments based on selection conditions --> make new fragments & add them
                    for (ComparisonExpression comp : fragCandidates) {
                        frags = makeFragmentationForComparisonExpression(comp);
                    }
                }

            } else {
                System.out.println("Found no fragment candidates for the given query (for selection conditions) ... ");
                System.exit(-1);
                // TODO query all servers and collect tuples on one server? store tuples to "collection" server or just answer the query?
            }

        } else {
            System.out.println("WHERE expression could not be analyzed! An error occured or maybe there is no WHERE...");
        }


//        // Process all joins found in the WHERE and FROM analysis
        HashMap<Join, Integer> copartitions = null;
        if (!joins.isEmpty()) {
            System.out.println("Found " + joins.size() + " joins. Testing joins for co-partitions ...");
            copartitions = testJoinsForCopartitions();
            for (Join j : copartitions.keySet()) {
                System.out.println("\t -> Join: " + j + ", copartitionID: " + copartitions.get(j));
            }
        } else {
            System.out.println("Found no joins in WHERE and FROM clause ...");
            // TODO So only selection from one table? Or something else?

        }


        // Query and fragments
        ArrayList<Query> subqueries = new ArrayList<>();
        if (frags == null || frags.isEmpty()) {
            // All fragments have to be considered (--> Todo get them and then distributed join?)
            System.out.println("There exists no fragmentation to the query so all fragments of tables in the query " +
                    "have to be considered to get an answer!");     // DEBUG
        } else {
            // There are fragments that match the query (selection condition)
            System.out.println("There are fragments that match the selection conditions of the query:");   //DEBUG
            for (Map.Entry<QualifiedName, Integer> entry : frags.entries()) {
                // Process the found fragments
                System.out.println("\t -> on " + entry.getKey() + " fragmentID=" + entry.getValue());    // DEBUG

                // TODO what if there are multiple selection conditions with multiple fragmentations?
                // Assumption: the found fragments cover the set of tuples to answer the query completely
                // --> result can be obtained as "Select .. From Tname1 .." + "Select .. From Tname2 .." + ...
                // if co-partitioned: "Select .. From Tname1, Tothername4 .." + ...
            }

            subqueries = rewriteQuery(queryBody, frags, copartitions);
        }
        for (Query q : subqueries) {    // DEBUG rewritten queries
            System.out.println(this.utils.queryToString(q));
        }


        return true;
    }



    /**
     * Process the where expression from the given {@link QuerySpecification}
     *
     * @param queryBody The body of a query
     * @return True if the WHERE expression in the body of the query could be analyzed; false otherwise
     */
    private boolean analyzeWhereExpression(@NotNull QueryBody queryBody) {

        // Try to get WHERE expression
        Expression where = this.utils.getWhere(queryBody);
        if (where == null) {
            System.out.println("Found no WHERE clause in the query!");
            return false;
        }
        System.out.println("Where-Expression: " + where);       // DEBUG


        // Tree debug
        //printAstTree(e);

        // traversal of the expression with the process method of superclass to classify subexpressions
        // --> this fills amongst other things the list of comparisons (for further processing)
        super.process(where);


        // Analyze the found comparisons & save information to process the joins in WHERE clause for co-partitioning
        // or to process selection conditions on (possibly) fragmented attributes to optimize the query
        System.out.println("Found the following comparisons in the WHERE expression: " + where);
        for (ComparisonExpression comp : comparisons) {
            System.out.print(comp + ", processing comparison ... ");
            processComparisonExpression(comp);
        }

        return true;
    }


    /**
     * Process the given comparison:
     * - Identify joins of two tables, e.g. T.a = S.b
     * - Identify attribute-value-comparisons, e.g. T.a = 5 or S.b <= 14.5677
     *
     * @param comp Comparison expression to be processed
     */
    private void processComparisonExpression(@NotNull ComparisonExpression comp) {

        // Disassemble comparison
        Expression left, right;
        left = comp.getLeft();
        right = comp.getRight();
        Operator op = comp.getOperator();

        if (left instanceof DereferenceExpression && right instanceof DereferenceExpression) {

            DereferenceExpression dref_left, dref_right;
            dref_left = (DereferenceExpression) left;
            dref_right = (DereferenceExpression) right;

            if (op.equals(Operator.EQUAL)) {         // IMPLICIT JOIN of left and right DereferenceExpressions
                System.out.println("Found an IMPLICIT JOIN! (will be stored as INNER JOIN because an IMPLICIT JOIN " +
                        "cannot have JoinCriteria ...)");

                // Get columns & create JoinCriteria (maybe not needed ...)
                List<Identifier> columns = new ArrayList<>();
                columns.add(dref_left.getField());
                columns.add(dref_right.getField());
                JoinCriteria joinCriteria = new JoinUsing(columns);  // TODO maybe change column list creation

                // Store join
                joins.add(new Join(Join.Type.INNER, new Table(QualifiedName.of(dref_left.getBase().toString())),
                        new Table(QualifiedName.of(dref_right.getBase().toString())), Optional.of(joinCriteria)));
            }
            // else Operator is of some other type: <, <=, >=, >, <>, ...

        } else if (left instanceof DereferenceExpression) {     // Some comparison of <attr> <op> <val>???
            System.out.println("Found a value comparison (left)");
            // Store it to analyze for matching fragmentations of that table
            fragCandidates.add(comp);

        } else if (right instanceof DereferenceExpression) {    // Some comparison of <val> <op> <attr>???
            System.out.println("Found a value comparison (right)");
            // Flip the comparison & store it to analyze for matching fragmentations of that table
            fragCandidates.add(new ComparisonExpression(op.flip(), right, left));

        } else {
            System.err.println("Don't know how to process the ComparisonExpression: " + comp);
        }

    }


    /**
     * This method analyzes the FROM expression in the given query body whether it contains a join of tables or not. It
     * will have a look at the JOIN of (two or more) tables (if present) and will store joins for later analysis in
     * a list.
     *
     * @param queryBody Body of the query to obtain FROM clause from
     * @return True if the FROM expression was successfully analyzed; false if there is no join
     */
    private boolean analyzeFromExpression(@NotNull QueryBody queryBody) {

        // Try to get FROM
       Relation from = this.utils.getFrom(queryBody);
        if (from == null)
            throw new IllegalArgumentException("No FROM expression found in the query body: " + queryBody);

        // Try to get join(s), if not present there is no join of two or more tables --> no need for co-partitioning
        if (from instanceof Join) {
            System.out.println("Found a join: " + (Join) from);        // DEBUG
            this.processJoin((Join) from);
            return true;
        } else {
            return false;
        }

        // TODO what is with 3+ tables being joined in FROM? Is the structure TA Join (TB Join TC) or how else?

    }


    /**
     * Process the Join:
     * - check if it is an IMPLICIT JOIN (--> analysis in WHERE)
     * - otherwise get Criteria of the Join (column information) if present and transform JoinOn to JoinUsing
     * (if necessary)
     *
     * @param join Join to process
     */
    private void processJoin(@NotNull Join join) {

        if (join.getType().equals(Join.Type.IMPLICIT)) {
            // The join is implicit => cf. WHERE expression analysis
            return;
        } else {

            JoinCriteria criteria = this.utils.getJoinCriteria(join);
            if (criteria != null) {
                if (criteria instanceof JoinOn) {
                    // Disassemble expression e (--> ComparisonExpression with DerefExpr. = DerefExpr.) & process it
                    // This transforms the JoinOn to a JoinUsing with column list
                    JoinOn joinOn = (JoinOn) criteria;
                    Expression e = joinOn.getExpression();
                    System.out.println("JoinOn: " + joinOn + ", expr: " + e);        // DEBUG

                    if (!(e instanceof ComparisonExpression))
                        throw new IllegalArgumentException("The expression " + e + " of the JoinOn " + joinOn +
                                " from the join " + join + " is not a ComparisonExpression, but an instance of " +
                                "class " + e.getClass().getName());

                    processComparisonExpression((ComparisonExpression) e);
                } else {
                    joins.add(join);    // add it to the list of joins
                }
            }

            // TODO what if criteria not present (add also to joins?)

        }

    }



// ########################################## Query rewriting ###############################################



    private ArrayList<Query> rewriteQuery(QueryBody queryBody, ListMultimap<QualifiedName, Integer> frags,
                                    HashMap<Join, Integer> copartitions) {

        ArrayList<Query> result = new ArrayList<>();

        // Reject if more than one attribute is considered --> distributed join (for now)   // TODO
        Set<QualifiedName> attributeRefs = frags.keySet();
        if (attributeRefs.size() > 1)
            throw new UnsupportedOperationException("Cannot rewrite the query body " + queryBody + " because there " +
                    "are fragments for multiple selection conditions required!");


        // Get & decompose attributeref (the only entry in the set)
        QualifiedName attributeref = attributeRefs.toArray(new QualifiedName[1])[0];
        List<Integer> fragIDs = frags.get(attributeref);
        String tableName = attributeref.getPrefix().get().getSuffix();
        String attrname = attributeref.getSuffix();

        // Compose rewritten Query from Select, From-Relation (Join or Table), Where-Expr.
        Select select = this.utils.getSelect(queryBody);
        Relation from = this.utils.getFrom(queryBody);
        Expression where = this.utils.getWhere(queryBody);

        // Rewrite SELECT for fragID
        HashMap<Integer, Select> selects = new HashMap<>();
        for (int fragID : fragIDs)
            selects.put(fragID, rewriteSelect(select, attributeref, fragID));

        // Rewrite FROM & WHERE clause based on found fragment and joins and copartitions
        ArrayList<Relation> froms = new ArrayList<>();
        if (copartitions == null || copartitions.isEmpty()) {
            for (int fragID : fragIDs) {
                froms.add(rewriteNoCopartitions(from, attributeref, fragID));
            }
        } else {

            // TODO there are co-partitions --> there are more than one tables
            if (joins.isEmpty()) {
                throw new IllegalStateException("There are co-partitions for the analyzed query but no joins of them." +
                        " This is an invalid state that should not occur!");
            }


        }


        // TODO generate resulting rewritten queries
        for (Select newSelect : selects)        // DEBUG
            System.out.println(newSelect);
        for (Relation newFrom : froms)          // DEBUG
            System.out.println(newFrom);
        return result;
    }



    private Select rewriteSelect(Select oldSelect, QualifiedName attributeref, int fragID) {

        ArrayList<SelectItem> newItems = new ArrayList<>();

        for (SelectItem item : oldSelect.getSelectItems()) {

            if (item instanceof SingleColumn) {

                SingleColumn column = (SingleColumn) item;
                Identifier alias = column.getAlias().orElse(new Identifier(""));
                Expression e = column.getExpression();
                System.out.println(e + " is a " + e.getClass());

                // TODO match expression (-> DereferenceExpr.?) with table name and fragment ID


            } else if (item instanceof AllColumns) {

                // if a prefix is present, e.g. TA.*, try to match and rewrite it; otherwise ignore & copy to new select
                AllColumns asterisk = (AllColumns) item;
                if (asterisk.getPrefix().isPresent()) {
                    QualifiedName prefix = asterisk.getPrefix().get();
                    if (prefix.equals(attributeref)) {
                        asterisk = new AllColumns(QualifiedName.of(prefix + "_" + fragID));
                    }
                }
                newItems.add(asterisk);

            } else
                throw new IllegalArgumentException("SelectItem " + item + " in Select object " + oldSelect + " is of " +
                        "type " + item.getClass());
        }
        return new Select(oldSelect.isDistinct(), newItems);
    }



    private Relation rewriteNoCopartitions(Relation from, QualifiedName attributeref, Integer fragID) {

        Relation result = null;

        // Decompose attributereference
        String tableName = attributeref.getPrefix().get().getSuffix();
        String attrname = attributeref.getSuffix();

        // check if there are joins
        if (this.joins.isEmpty()) {

            // No joins --> check if the query is concerning a single table
            if (from instanceof Table) {

                // Rewrite table in FROM to TABLENAME_fragID if table matches, otherwise ignore it
                Table tab = (Table) from;
                if (tableName.equals(tab.getName().toString())) {
                    QualifiedName newName = QualifiedName.of(tab.getName() + "_" + fragID);
                    result = new Table(newName);
                }

            } else {
                // There are two or more tables combined by a natural join
                // TODO Natural join rewriting? Is this necessary?
            }

        } else {
            // There are joins and so multiple tables ...
            // TODO if more tables but not co-partitioned?
        }

        return result;
    }



// ########################### Metadata information ################################


    /**
     * This method tests the fragment candidates identified for the analyzed query to obtain the relevant fragment ids.
     *
     * @return All fragments that need to be accessed when executing the query. Entries are of the form:
     * [attribute-reference, fragment-id], e.g. [table.attribute, 125], where each attribute-reference can have multiple
     * required fragment IDs.
     */
    private ListMultimap<QualifiedName, Integer> testFragCandidates() {

        QualifiedNameComparator comparator = new QualifiedNameComparator();

        // Match information about fragmentations (FRAGMENTA) with the query attribute-value-comparisons
        // This multimap stores for each attribute name all attribute-value-comparison
        ListMultimap<QualifiedName, ComparisonExpression> attrComps =
                MultimapBuilder.treeKeys(comparator).arrayListValues().build();

        // This multimap stores for each attribute all fragment ids that match the comparisons
        ListMultimap<QualifiedName, Integer> result = MultimapBuilder.treeKeys(comparator).arrayListValues().build();

        // Store all comparisons into the multimap with their attribute reference (table.attributename) as key
        for (ComparisonExpression comp : fragCandidates) {
            DereferenceExpression left = (DereferenceExpression) comp.getLeft();
            attrComps.put(DereferenceExpression.getQualifiedName(left), comp);
        }

        // For every attribute test if it is compared once or several times
        for (QualifiedName attributeRef : attrComps.keySet()) {

            List<ComparisonExpression> list = attrComps.get(attributeRef);
            if (list.size() == 1) {     // only once compared

                // Get all the fragments matching this comparison
                ArrayList<Integer> fragmentIDs = getFragsOfComparison(list.get(0));
                if (!fragmentIDs.isEmpty()) {
                    result.putAll(attributeRef, fragmentIDs);
                }

            } else if (list.size() == 2) {      // twice compared, e.g. T.a >= 15 AND T.a <= 20

                // get the comparisons
                ComparisonExpression a, b;
                a = list.get(0);
                b = list.get(1);
                result.putAll(attributeRef, getFragsOfComparisons(a, b));


            } else throw new IllegalArgumentException("Cannot process the comparisons for the attribute " + attributeRef
                    + " because there are too many comparisons on this attribute. Either the conditions are redundant"
                    + " or the query might fail anyways due to contradictory selection conditions!");

        }

        // return the found assignment for the comparisons
        return result;
    }


    /**
     * This method checks if a comparison to a value (e.g. T.AGE >= 5) matches a given range.
     * NOTE: Comparison with "IS DISTINCT FROM' results in false always
     *
     * @param op        Comparison operator
     * @param compvalue Comparison value
     * @param minvalue  Range minimal value
     * @param maxvalue  Range maximal
     * @return Result of the check
     */
    private boolean matchCompAndFragMeta(Operator op, long compvalue, long minvalue, long maxvalue) {
        switch (op) {
            case EQUAL:
                return (minvalue <= compvalue && compvalue <= maxvalue);
            case NOT_EQUAL:
                return (minvalue > compvalue && compvalue > maxvalue);
            case LESS_THAN:
                return (minvalue < compvalue);
            case LESS_THAN_OR_EQUAL:
                return (minvalue <= compvalue);
            case GREATER_THAN:
                return (maxvalue > compvalue);
            case GREATER_THAN_OR_EQUAL:
                return (maxvalue >= compvalue);
            case IS_DISTINCT_FROM:
        }
        return false;
    }


    /**
     * This method gets the fragments matching the selection condition in form of two comparisons on the same attribute
     *
     * @param a ComparisonExpression of the form T.x op value
     * @param b ComparisonExpression of the form T.x op2 value2
     * @return Returns a list which contains the fragment ids
     */
    private ArrayList<Integer> getFragsOfComparisons(ComparisonExpression a, ComparisonExpression b) {

        ArrayList<Integer> result = new ArrayList<>();

        // The resulting list of fragments is the intersection of the two fragment lists of both comparisons alone
        ArrayList<Integer> afrags = getFragsOfComparison(a);
        ArrayList<Integer> bfrags = getFragsOfComparison(b);
        for (Integer aid : afrags) {
            if (bfrags.contains(aid))
                result.add(aid);
        }

        // TODO might be the case that there is no fragmentation present
        // if (afrags.isEmpty() || bfrags.isEmpty()) ...

        // If the intersection of both is empty, the two comparisons are unsatisfiable together (e.g. x>5 && x<0)
        if (result.isEmpty())
            throw new IllegalArgumentException("The intersection of the two fragment lists of comparisons " + a
                    + " and " + b + " is empty because the selection conditions are unsatisfiable which makes the" +
                    " query unsatisfiable!");
        return result;
    }


    /**
     * This method returns all fragments matching the given comparison
     *
     * @param comp The comparison
     * @return All fragment ids that match the comparison
     */
    private ArrayList<Integer> getFragsOfComparison(ComparisonExpression comp) {

        ArrayList<Integer> result = new ArrayList<>();

        // Get the tablename and attributename
        String tablename, attrname;
        String attributeref =
                DereferenceExpression.getQualifiedName((DereferenceExpression) comp.getLeft()).toString().toUpperCase();
        tablename = attributeref.split("\\.")[0];      // attributeref = <table>.<attribute> (hopefully)
        attrname = attributeref.split("\\.")[1];

        // get operator and value
        Operator op = comp.getOperator();
        Expression right = comp.getRight();
        long compvalue;
        if (right instanceof LongLiteral) {
            LongLiteral longLit = (LongLiteral) right;
            compvalue = longLit.getValue();
        } else throw new IllegalArgumentException("The value of the comparison " +
                "is not a Long but a " + right.getClass().getName());


        // Query the fragmentation meta data table with the table and attribute name
        String sql = "SELECT ID,MINVALUE,MAXVALUE FROM FRAGMETA WHERE TABLE=? AND ATTRIBUTE=?";
        PreparedStatement prep;
        try {
            prep = conn.prepareStatement(sql);
            prep.setString(1, tablename);
            prep.setString(2, attrname);
            ResultSet res = prep.executeQuery();

            // If a fragment range matches, then put it into the result
            while (res.next()) {
                int fragmentID = res.getInt(1);
                if (matchCompAndFragMeta(op, compvalue, res.getInt(2), res.getInt(3))) {
                    result.add(fragmentID);
                }
            }
        } catch (SQLException e) {
            System.err.println("ERROR for PreparedStatement to query FRAGMETA in " + this.getClass().getName());
            e.printStackTrace();
        }

        return result;
    }



    private HashMap<Join, Integer> testJoinsForCopartitions() {

        // Test the joins from the WHERE clause, e.g. WHERE ... AND T.a = S.b AND ..., whether the joined tables are
        // co-partitioned on that attribute(s); if a co-partitioning is found, then the join is added with the id of the
        // metadata tuple to the result HashMap

        HashMap<Join, Integer> result = new HashMap<>();

        for (Join join : joins) {

            // Get the tablenames and attribute names (join should have tables t1.a1 and t2.a2 if only join of 2 tables)
            Table left, right;
            left = (Table) join.getLeft();
            right = (Table) join.getRight();
            String table, cotable, joinattr, cojoin;
            // Table names
            table = left.getName().toString().toUpperCase();
            cotable = right.getName().toString().toUpperCase();
            // Attribute names
            JoinCriteria criteria = this.utils.getJoinCriteria(join);
            if (criteria != null) {
                List<Identifier> joinColumns = ((JoinUsing) criteria).getColumns();
                joinattr = joinColumns.get(0).getValue();
                cojoin = joinColumns.get(1).getValue();
                // TODO maybe check size --> 3+ tables joined 3+ columns in list?!
                // TODO maybe assign attribute names not according to position in arraylist but according to tablename?
            } else {
                throw new IllegalStateException("The JoinCriteria of Join " + join + " must not be null!");
            }

            // Query the co-partitioning meta data table with the table and attribute names
            String sql = "SELECT ID FROM COMETA WHERE TABLE=? AND JOINATTR = ? AND COTABLE=? AND COJOIN=?";
            PreparedStatement prep;
            try {
                prep = conn.prepareStatement(sql);
                prep.setString(1, table);
                prep.setString(2, joinattr);
                prep.setString(3, cotable);
                prep.setString(4, cojoin);
                ResultSet res = prep.executeQuery();

                // If a co-partitioning is found, then store join & the co-partitioning id
                if (res.next()) {
                    int copartID = res.getInt(1);
                    result.put(join, copartID);
                    continue;
                }

                // Otherwise, check the other direction (join is bidirectional); and if nothing is found here, there is
                // no co-partitioning and so store the join together with a null value to the result
                prep.setString(1, cotable);
                prep.setString(2, cojoin);
                prep.setString(3, table);
                prep.setString(4, joinattr);
                res = prep.executeQuery();
                if (res.next()) {
                    int copartID = res.getInt(1);
                    result.put(join, copartID);
                } else {
                    result.put(join, null);
                }

            } catch (SQLException e) {
                System.err.println("ERROR for PreparedStatement to query FRAGMETA in " + this.getClass().getName());
                e.printStackTrace();
            }
        }

        return result;
    }


    /**
     * Generates the ordered list with all column names and their SQL-Types from a certain table by querying the
     * database in the following form:
     * List = ["Column1;TypeA", "Column2;TypeB", ...]
     * TODO in this way "super" table has to be created always; maybe other solution more appropriate?
     * @param table Table name
     * @return Column List
     */
    private ArrayList<String> getColumnsFromTable(String table) {

        ArrayList<String> columns = new ArrayList<>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet res = stmt.executeQuery("SELECT * FROM " + table);     // Dummy query to get metadata

            ResultSetMetaData meta = res.getMetaData();
            for (int i = 1; i <= meta.getColumnCount(); i++) {
                columns.add(meta.getColumnName(i) + ";" + meta.getColumnTypeName(i));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columns;
    }


    /**
     * This method returns a list of all primary column names of a certain table
     * TODO in this way "super" table has to be created always; maybe other solution more appropriate?
     * @param table Table name
     * @return Primary key columns
     */
    private ArrayList<String> getPrimaryKeysFromTable(String table) {
        ArrayList<String> primaryKeys = new ArrayList<>();
        try {
            ResultSet res = conn.getMetaData().getPrimaryKeys("", "", table);
            while (res.next()) {
                primaryKeys.add(res.getString("PK_NAME"));      // Should be "COLUMN_NAME"? maybe Ignite ...
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return primaryKeys;
    }


// ########################## Fragmentation Management ##############################################

    // TODO maybe as own class?

    /**
     * This method will create a new fragmentation for the given comparison expression (which is a selection condition
     * in the currently analyzed query). The ComparisonExpression has to be of the form '<DereferenceExpression> <op>
     * <Value>'. For the sake of simplicity, the newly created fragmentation will consist of two selection conditions:
     * the one given as argument to this function and the negated version of it (other direction).
     * After this, the method will rearrange the data to match the fragmentation and update the metadata.
     * It is assumed that there is no fragmentation present for the given table attribute combination!
     *
     * @param comp Comparison to make a new fragmentation from
     */
    private ListMultimap<QualifiedName, Integer> makeFragmentationForComparisonExpression(ComparisonExpression comp) {

        QualifiedNameComparator comparator = new QualifiedNameComparator();
        ListMultimap<QualifiedName, Integer> result = MultimapBuilder.treeKeys(comparator).arrayListValues().build();

        // Decompose ComparisonExpression
        DereferenceExpression left = (DereferenceExpression) comp.getLeft();
        String table = left.getBase().toString().toUpperCase();
        String attribute = left.getField().getValue().toUpperCase();
        Expression right = comp.getRight();
        int value = (int) ((LongLiteral) right).getValue();
        Operator op = comp.getOperator();
        Operator negop = op.negate();

        // Update the metadata & sql-create string for fragment table & add to result
        Integer[] minMax = setMinMaxValue(op, value);
        int fragID = metaMakeNewFragment(table, attribute, minMax[0], minMax[1]);
        String create = buildFragmentCreateString(table, fragID);
        result.put(QualifiedName.of(table, attribute) , fragID);

        // Update metadata for the second negated fragment & sql-create string (not added because not matching query)
        minMax = setMinMaxValue(negop, value);
        fragID = metaMakeNewFragment(table, attribute, minMax[0], minMax[1]);
        String negCreate = buildFragmentCreateString(table, fragID);

        // TODO Create the two fragment tables
        try {
            Statement stmt = conn.createStatement();
            //stmt.execute(create);
            //stmt.execute(negCreate);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        // TODO rearrangement
        // Rearrange the data according to the new fragmentation: insert data into the new fragments from all 'old'
        // fragments matching the selection condition of the fragment
        String insert = buildFragmentInsertString(table, fragID);
        try {
            PreparedStatement prep = conn.prepareStatement(insert);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }


    /**
     * Update the metadata and set a new fragment according to the arguments. If minvalue is null, it means that for all
     * tuples in the fragment holds that attribute <= maxvalue, and if maxvalue is null, for all tuples in the fragment
     * holds that attribute >= minvalue. If both are null, this is an error!
     *
     * @param table     Name of the table
     * @param attribute Name of the attribute
     * @param minvalue  Minimum value of the fragment (inclusively)
     * @param maxvalue  Maximum value of the fragment (inclusively)
     * @return The ID of the fragment (name is 'table_ID'), -1 on error
     */
    private int metaMakeNewFragment(String table, String attribute, Integer minvalue, Integer maxvalue) {

        if (minvalue == null && maxvalue == null)
            throw new IllegalArgumentException("The both arguments minvalue and maxvalue must not be null at the same" +
                    " time!");


        // Find the next ID to be used to store a fragment of this table
        int nextID = 0;
        try {
            PreparedStatement prep = conn.prepareStatement("SELECT MAX(ID) FROM FRAGMETA WHERE TABLE = ?");
            prep.setString(1, table);
            ResultSet res = prep.executeQuery();
            if (res.next())
                nextID = res.getInt(1) + 1;


            // Store the fragment in the metadata table (with NULL value for minvalue or maxvalue if needed)
            prep = conn.prepareStatement("INSERT INTO FRAGMETA (ID, TABLE, ATTRIBUTE, MINVALUE, MAXVALUE) VALUES " +
                    "(?,?,?,?,?)");
            prep.setInt(1, nextID);
            prep.setString(2, table);
            prep.setString(3, attribute);
            if (minvalue == null)
                prep.setNull(4, Types.INTEGER);
            else
                prep.setInt(4, minvalue);
            if (maxvalue == null)
                prep.setNull(5, Types.INTEGER);
            else
                prep.setInt(5, maxvalue);

            // Execute update
            int rowsupdated = prep.executeUpdate();
            if (rowsupdated != 1)
                nextID = -1;    //todo exception?
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return nextID;

    }


    /**
     * This method computes the minvalue and maxvalue to a given operator and a value.
     *
     * @param op    Operator
     * @param value Value
     * @return Integer[] = {minvalue, maxvalue}
     */
    private Integer[] setMinMaxValue(Operator op, Integer value) {
        Integer minvalue, maxvalue;
        minvalue = null;
        maxvalue = null;
        switch (op) {
            case LESS_THAN:
                maxvalue = value - 1;
                break;
            case LESS_THAN_OR_EQUAL:
                maxvalue = value;
                break;
            case GREATER_THAN:
                minvalue = value + 1;
                break;
            case GREATER_THAN_OR_EQUAL:
                maxvalue = value;
                break;
            default:
                throw new IllegalArgumentException("The given setMinMaxValue-Operation is not valid because it " +
                        "contains an operator which is not <, <=, >, >=!");
        }
        return new Integer[]{minvalue, maxvalue};
    }


    /**
     * This method builds the CREATE TABLE SQL statement which will be used for creation of a fragment of a given table
     *
     * @param table  Table name
     * @param fragID ID of the fragment (used in name of the fragment table)
     * @return SQL CREATE TABLE statement
     */
    private String buildFragmentCreateString(String table, int fragID) {

        // Get the columns and primary keys
        ArrayList<String> columns, primaryKeys;
        columns = getColumnsFromTable(table);
        primaryKeys = getPrimaryKeysFromTable(table);

        // Add the column list to the build
        StringBuilder create = new StringBuilder("CREATE TABLE " + table + "_" + fragID + " (");
        for (String s : columns) {
            create.append(s.replace(";", " ")).append(", ");
        }

        // Add the primary key to the build
        create.append("PRIMARY KEY (");
        if (primaryKeys.size() < 1) {
            // todo exception? or what?
        }
        create.append(primaryKeys.remove(0));
        for (String s : primaryKeys) {
            create.append(", ").append(s);
        }
        create.append(") )");

        // return the build
        return create.toString();
    }


    /**
     * Builds an INSERT INTO SQL statement for a given table fragment
     *
     * @param table  Table name
     * @param fragID ID of the fragment (used in table name)
     * @return SQL INSERT statement
     */
    private String buildFragmentInsertString(String table, int fragID) {

        // Start to build
        StringBuilder insert = new StringBuilder("INSERT INTO " + table + "_" + fragID + " (");

        // Add column names of the table to the build
        ArrayList<String> columns = getColumnsFromTable(table);
        if (columns.size() < 1)
            ;   // todo what to do? excpetion?
        insert.append(columns.remove(0).split(";")[0]);
        for (String s : columns) {
            insert.append("," + s.split(";")[0]);       // only name needed; type irrelevant here
        }
        insert.append(") ");

        // Add subquery with selection condition for fragment
        insert.append("(SELECT * FROM " + table + " WHERE )");    // todo condition

        // return build
        System.out.println(insert.toString());      // DEBUG
        return insert.toString();
    }


// ################################ DEBUG Stuff ###########################################

    /**
     * Prints the AST-Tree to the given expression to {@link System#out}
     *
     * @param e Expression
     */
    private void printAstTree(Expression e) {
        IdentityHashMap<Expression, QualifiedName> ihm = new IdentityHashMap<>();
        ihm.put(e, QualifiedName.of(e.toString()));
        TreePrinter tp = new TreePrinter(ihm, System.out);
        tp.print(e);
    }


    /**
     * Test unit
     * @param args
     */
    public static void main(String[] args) {


    }

}
