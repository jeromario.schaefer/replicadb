import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;

import java.util.*;


public class StartIgniteClientNode {


    private Ignite ignite;

    private IgniteCluster cluster;

    public StartIgniteClientNode(IgniteConfiguration cfg) {
        this.ignite = Ignition.start(cfg);
        this.cluster = ignite.cluster();
        System.out.println("List of all Nodes:");
        for (ClusterNode node : this.cluster.nodes())
            System.out.println("\t"+node.hostNames()+"," + node.id());

        System.out.println("List of server Nodes:");
        for (ClusterNode node : this.cluster.forServers().nodes())
            System.out.println("\t"+node.hostNames()+"," + node.id());
    }


    public boolean stopClient() {
        return Ignition.stop(this.ignite.name(), true);
    }


    public void stopAllNodes() {
        this.cluster.stopNodes();
    }


    /**
     * Starts an ignite client node that connects to the cluster on keg01 and then connects to the cluster on keg07
     * @param args Not used
     */
    public static void main(String[] args) {

        // KEG01
        // Discovery
        TcpDiscoverySpi spi = new TcpDiscoverySpi();
        TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
        ipFinder.setAddresses(Arrays.asList("keg01.keg.loc:47500..47509"));
        spi.setIpFinder(ipFinder);

        // Configuration
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setDiscoverySpi(spi)
            .setPeerClassLoadingEnabled(true)
            .setClientMode(true);

        StartIgniteClientNode clientNode = new StartIgniteClientNode(cfg);
        clientNode.stopClient();


        // KEG07
        // Discovery
        spi = new TcpDiscoverySpi();
        ipFinder = new TcpDiscoveryVmIpFinder();
        ipFinder.setAddresses(Arrays.asList("keg07.keg.loc:48500..48509"));
        spi.setIpFinder(ipFinder);

        // Configuration
        cfg = new IgniteConfiguration();
        cfg.setDiscoverySpi(spi)
                .setPeerClassLoadingEnabled(true)
                .setClientMode(true);

        clientNode = new StartIgniteClientNode(cfg);
        clientNode.stopClient();
        System.out.println("Done");
    }
}

