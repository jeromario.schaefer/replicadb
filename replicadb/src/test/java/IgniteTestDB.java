import javax.swing.plaf.nimbus.State;
import java.sql.*;

public class IgniteTestDB {

    /**
     * Connect to the cluster on keg01 and afterwards to the cluster on keg07. Drops and creates fragment tables for the
     * both clusters, drops and creates metadata tables for both clusters (equally) and
     * @param args
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // Register driver
        Class.forName("org.apache.ignite.IgniteJdbcThinDriver");


        // KEG01
        // Connection to the cluster (Port 10800 default for JDBC client)
        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://keg01.keg.loc:10800")) {
            Statement stmt = conn.createStatement();

            // Drop and then create table fragments (TA_1, TA_3, TA_5, and accordingly TB_1, TB_3, TB_5)
            stmt.executeUpdate("DROP TABLE IF EXISTS TA_1; DROP TABLE IF EXISTS TA_2; DROP TABLE IF EXISTS TA_3; " +
                    "DROP TABLE IF EXISTS TA_4; DROP TABLE IF EXISTS TA_5");
            stmt.executeUpdate("DROP TABLE IF EXISTS TB_1; DROP TABLE IF EXISTS TB_2; DROP TABLE IF EXISTS TB_3; " +
                    "DROP TABLE IF EXISTS TB_4; DROP TABLE IF EXISTS TB_5");
            stmt.executeUpdate("CREATE TABLE TA_1 ( ID INT PRIMARY KEY, NAME VARCHAR, AGE INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TA_3 ( ID INT PRIMARY KEY, NAME VARCHAR, AGE INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TA_5 ( ID INT PRIMARY KEY, NAME VARCHAR, AGE INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TB_1 ( ID INT PRIMARY KEY, IDOFA INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TB_3 ( ID INT PRIMARY KEY, IDOFA INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TB_5 ( ID INT PRIMARY KEY, IDOFA INT) " +
                    "WITH \"template=replicated,backups=0\"");

            // Metadata table
            stmt.executeUpdate("DROP TABLE IF EXISTS FRAGMETA; DROP TABLE IF EXISTS COMETA;");
            stmt.executeUpdate("CREATE TABLE FRAGMETA (ID INT PRIMARY KEY, TABLE VARCHAR, ATTRIBUTE VARCHAR, " +
                    "MINVALUE INT, MAXVALUE INT) WITH \"template=replicated,backups=0\"");

            stmt.executeUpdate("CREATE TABLE COMETA (ID INT PRIMARY KEY, TABLE VARCHAR, JOINATTR VARCHAR, " +
                    "COTABLE VARCHAR, COJOIN VARCHAR) WITH \"template=replicated,backups=0\"");

            // Store some meta-info
            String insert = "INSERT INTO FRAGMETA (ID, TABLE, ATTRIBUTE, MINVALUE, MAXVALUE) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement prep = conn.prepareStatement(insert);
            for (int i = 0; i < 5; i++) {
                // 5 fragments for TA on TA.AGE: 1-20, 21-40, etc.
                prep.setInt(1, i);
                prep.setString(2, "TA");
                prep.setString(3, "AGE");
                prep.setInt(4, 20 * i + 1);
                prep.setInt(5, 20 * (i + 1));
                prep.executeUpdate();
            }

            // Some co-partitioned meta data
            insert = "INSERT INTO COMETA (ID, TABLE, JOINATTR, COTABLE, COJOIN) VALUES (?,?,?,?,?)";
            prep = conn.prepareStatement(insert);
            prep.setInt(1, 1732);
            prep.setString(2, "TB");
            prep.setString(3, "IDOFA");
            prep.setString(4, "TA");
            prep.setString(5, "ID");
            prep.executeUpdate();

            // Close
            prep.close();
            stmt.close();
        } finally {
            System.out.println("Finished KEG01!");
        }



        // KEG07
        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://keg07.keg.loc:10800")){
            Statement stmt = conn.createStatement();

            // Drop and then create table fragments (TA_2, TA_4, and accordingly TB_2, TB_4)
            stmt.executeUpdate("DROP TABLE IF EXISTS TA_1; DROP TABLE IF EXISTS TA_2; DROP TABLE IF EXISTS TA_3; " +
                    "DROP TABLE IF EXISTS TA_4; DROP TABLE IF EXISTS TA_5");
            stmt.executeUpdate("DROP TABLE IF EXISTS TB_1; DROP TABLE IF EXISTS TB_2; DROP TABLE IF EXISTS TB_3; " +
                    "DROP TABLE IF EXISTS TB_4; DROP TABLE IF EXISTS TB_5");
            stmt.executeUpdate("CREATE TABLE TA_2 ( ID INT PRIMARY KEY, NAME VARCHAR, AGE INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TA_4 ( ID INT PRIMARY KEY, NAME VARCHAR, AGE INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TB_2 ( ID INT PRIMARY KEY, IDOFA INT) " +
                    "WITH \"template=replicated,backups=0\"");
            stmt.executeUpdate("CREATE TABLE TB_4 ( ID INT PRIMARY KEY, IDOFA INT) " +
                    "WITH \"template=replicated,backups=0\"");

            // Metadata table (in each cluster)
            stmt.executeUpdate("DROP TABLE IF EXISTS FRAGMETA; DROP TABLE IF EXISTS COMETA;");
            stmt.executeUpdate("CREATE TABLE FRAGMETA (ID INT PRIMARY KEY, TABLE VARCHAR, ATTRIBUTE VARCHAR, " +
                    "MINVALUE INT, MAXVALUE INT) WITH \"template=replicated,backups=0\"");

            stmt.executeUpdate("CREATE TABLE COMETA (ID INT PRIMARY KEY, TABLE VARCHAR, JOINATTR VARCHAR, " +
                    "COTABLE VARCHAR, COJOIN VARCHAR) WITH \"template=replicated,backups=0\"");


            // Store some meta-info
            String insert = "INSERT INTO FRAGMETA (ID, TABLE, ATTRIBUTE, MINVALUE, MAXVALUE) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement prep = conn.prepareStatement(insert);
            for (int i = 0; i < 5; i++) {
                // 5 fragments for TA on TA.AGE: 1-20, 21-40, etc.
                prep.setInt(1, i);
                prep.setString(2, "TA");
                prep.setString(3, "AGE");
                prep.setInt(4, 20 * i + 1);
                prep.setInt(5, 20 * (i + 1));
                prep.executeUpdate();
            }

            // Some co-partitioned meta data
            insert = "INSERT INTO COMETA (ID, TABLE, JOINATTR, COTABLE, COJOIN) VALUES (?,?,?,?,?)";
            prep = conn.prepareStatement(insert);
            prep.setInt(1, 1732);
            prep.setString(2, "TB");
            prep.setString(3, "IDOFA");
            prep.setString(4, "TA");
            prep.setString(5, "ID");
            prep.executeUpdate();


            // Close
            prep.close();
            stmt.close();

        } finally {
            System.out.println("Finished KEG07!");
        }



        // Now test with the QueryAnalyzer
        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://localhost:10800")) {
            String sql = "SELECT * FROM TA, TB WHERE 40 >= TA.AGE AND TA.AGE > 15 AND TA.ID = TB.IDOFA";

            // Analyze the query
            System.out.println(" --------------- Analysis --------------- ");
            QueryAnalyzer analyzer = new QueryAnalyzer(conn);
            analyzer.analyzePrint(sql);
            System.out.println("\n");


        } finally {
            System.out.println("Finished Analysis!");
        }
    }
}
